﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace ApiNest.Domain
{
    [SuppressMessage("ReSharper", "ClassWithVirtualMembersNeverInherited.Global")]
    public class Mashup
    {
        [Key]
        public string Id { get; internal set; }

        public string Name { get; internal set; }
        public virtual ApplicationUser Owner { get; internal set; }

        public virtual ICollection<ProxyMiddlewareDefinition> MiddlewareProxies { get; internal set; } =
            new List<ProxyMiddlewareDefinition>();

        public virtual ApiRegistration TargetApi { get; internal set; }



        public class ProxyMiddlewareDefinition
        {
            [Key]
            public string Id { get; internal set; }

            public virtual Mashup Mashup { get; internal set; }
            public virtual ProxyRegistration Proxy { get; internal set; }

        }


        public void UpdateDetails(string name, ApiRegistration targetApi, ICollection<ProxyRegistration> proxies)
        {
            this.Name = name;
            this.TargetApi = targetApi;
            MiddlewareProxies.Clear();
            MiddlewareProxies =
              proxies.Select((registration) => new Mashup.ProxyMiddlewareDefinition()
              {
                  Id = Guid.NewGuid().ToString(),
                  Mashup = this,
                  Proxy = registration
              }).ToList();
        }
    }
}