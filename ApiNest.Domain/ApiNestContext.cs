﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ApiNest.Domain
{
    public class ApiNestContext :  IdentityDbContext<ApplicationUser>, IApiNestSession
    {

        public ApiNestContext() : base("DefaultConnection")
        {
        }

        public DbSet<ApiRegistration> ApiRegistrations { get; set; }
        public DbSet<ProxyRegistration> ProxyRegistrations { get; set; }
        public DbSet<Mashup> Mashups { get; set; }

        IQueryable<ApiRegistration> IApiNestSession.ApiRegistrations => this.ApiRegistrations;
        IQueryable<ProxyRegistration> IApiNestSession.ProxyRegistrations => this.ProxyRegistrations;
        IQueryable<Mashup> IApiNestSession.Mashups => this.Mashups;
        IQueryable<ApplicationUser> IApiNestSession.Users=> this.Users;

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

        }

        public Mashup AddMashup(string name, ApiRegistration target, ProxyRegistration[] proxyRegistrations, ApplicationUser currentUser)
        {
            var mashup = new Mashup
            {
                Name = name,
                Id = Guid.NewGuid().ToString()
            };
            mashup.MiddlewareProxies =
                proxyRegistrations.Select((registration) => new Mashup.ProxyMiddlewareDefinition()
                {
                    Id = Guid.NewGuid().ToString(),
                    Mashup = mashup,
                    Proxy = registration
                }).ToArray();


            mashup.Owner = currentUser;
            mashup.TargetApi = target;
            this.Mashups.Add(mashup);
            return mashup;
        }

        public void AddApiRegistration(string name, string entryPointUrl, ApplicationUser currentUser)
        {
            this.ApiRegistrations.Add(new ApiRegistration()
            {
                Id = Guid.NewGuid().ToString(),
                EntryPointUrl = entryPointUrl,
                Name = name,
                Owner = currentUser
            });
        }
 

        public void AddProxyRegistration(string name, string entryPointUrl, ApplicationUser currentUser)
        {
            this.ProxyRegistrations.Add(new ProxyRegistration()
            {
                EntryPointUrl = entryPointUrl,
                Name = name,
                Owner = currentUser,
                Id = Guid.NewGuid().ToString()

            });
            
        }
    }

    public interface IApiNestSession : IDisposable
    {
        IQueryable<ApiRegistration> ApiRegistrations { get; }
        IQueryable<ProxyRegistration> ProxyRegistrations { get; }
        IQueryable<Mashup> Mashups { get; }
        IQueryable<ApplicationUser> Users { get; }

        Mashup AddMashup(string name, ApiRegistration target, ProxyRegistration[] proxyRegistrations, ApplicationUser currentUser);
        void AddProxyRegistration(string name, string entryPointUrl, ApplicationUser currentUser);
        void AddApiRegistration(string name, string entryPointUrl, ApplicationUser currentUser);
        int SaveChanges();
    }
}