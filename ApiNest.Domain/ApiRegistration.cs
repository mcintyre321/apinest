﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace ApiNest.Domain
{
    [SuppressMessage("ReSharper", "ClassWithVirtualMembersNeverInherited.Global")]
    public class ApiRegistration
    {
        [Key]
        public string Id { get; internal set; }

        public string Name { get; internal set; }
        public string EntryPointUrl { get; internal set; }
        public virtual ApplicationUser Owner { get; internal set; }

        public void UpdateDetails(string name, string entryPointUrl)
        {
            this.Name = name;
            this.EntryPointUrl = entryPointUrl;
        }
    }
}
