﻿using System.ComponentModel.DataAnnotations;

namespace ApiNest.Domain
{
    // ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
    public class ProxyRegistration
    {
        [Key]
        public string Id { get; internal set; }
        public string Name { get; internal set; }
        public string EntryPointUrl { get; internal set; }
        public virtual ApplicationUser Owner { get; internal set; }

        public void UpdateDetails(string name, string entryPointUrl)
        {
            this.Name = name;
            this.EntryPointUrl = entryPointUrl;
        }
    }
}