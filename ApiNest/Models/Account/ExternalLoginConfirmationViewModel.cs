﻿using System.ComponentModel.DataAnnotations;

namespace ApiNest.Models.Account
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
