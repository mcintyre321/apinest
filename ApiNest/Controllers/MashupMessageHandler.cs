﻿using System;
using System.Data.Entity;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Routing;
using ApiNest.Domain;

namespace ApiNest.Controllers
{
    public class MashupMessageHandler : HttpMessageHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            IHttpRouteData routeData = request.GetRouteData();
            Contract.Assert(routeData != null);

            var id = Guid.ParseExact(routeData.Values["mashupId"].ToString(), "N").ToString();
            ;
            using (var db = new ApiNestContext())
            {

                var mashup = db.Mashups.Where(m => m.Id == id)
                    .Include(m => m.MiddlewareProxies.Select(mp => mp.Proxy))
                    .Single();
                
                var proxies = mashup.MiddlewareProxies.Select(m => m.Proxy.EntryPointUrl)
                    .Select(p => new Uri(p))
                    .Select(p => p.GetComponents(UriComponents.SchemeAndServer, UriFormat.SafeUnescaped))
                    .ToArray();


                var endwareUrl = new Uri(mashup.TargetApi.EntryPointUrl);
                var destinations = proxies
                    .Concat(new[] {endwareUrl.GetComponents(UriComponents.SchemeAndServer, UriFormat.SafeUnescaped)});
                request.Headers.Add("X-Destination", destinations.Skip(1));

                //var userIp = HttpContext.Current.Request.UserHostAddress;
                //request.Headers.Add("X-Forwarded-For", userIp);
                if (request.Method == HttpMethod.Get || request.Method == HttpMethod.Trace) request.Content = null;

                var nextRequestUri = new UriBuilder(proxies.First())
                {
                    Path = System.Web.Utility.UrlDecode(request.RequestUri.PathAndQuery)
                };

                request.Headers.Host = nextRequestUri.Host;
                request.RequestUri = new Uri(nextRequestUri.ToString());
                request.Headers.AcceptEncoding.Clear();

                var responseMessage = await
                    new HttpClient().SendAsync(request, HttpCompletionOption.ResponseHeadersRead,
                        cancellationToken);
                responseMessage.Headers.TransferEncodingChunked = null; //throws an error on calls to WebApi results
                if (request.Method == HttpMethod.Head) responseMessage.Content = null;
                return responseMessage;
            }
        }
    }
}