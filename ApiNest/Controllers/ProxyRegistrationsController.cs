﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ApiNest.Domain;
using Microsoft.AspNet.Identity;

namespace ApiNest.Controllers
{
    
    public class ProxyRegistrationsController : Controller
    {
        private IApiNestSession db = new ApiNestContext();

        // GET: ProxyRegistrations
        public ActionResult Index()
        {
            return View(db.ProxyRegistrations.ToList());
        }

        // GET: ProxyRegistrations/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProxyRegistration proxyRegistration = db.ProxyRegistrations.Single(x => x.Id == id);
            if (proxyRegistration == null)
            {
                return HttpNotFound();
            }
            return View(new ProxyRegistrationVm(proxyRegistration));
        }

        // GET: ProxyRegistrations/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "Name,EntryPointUrl")] ProxyRegistrationVm proxyRegistration)
        {
            if (ModelState.IsValid)
            {
                var userId = HttpContext.User.Identity.GetUserId();
                var applicationUser = db.Users.Single(u => u.Id == userId);
                db.AddProxyRegistration(proxyRegistration.Name, proxyRegistration.EntryPointUrl, applicationUser);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(proxyRegistration);
        }

        [Authorize]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userId = HttpContext.User.Identity.GetUserId();
            var proxyRegistration = db.Users
                .Where(u => u.Id == userId)
                .SelectMany(u => u.OwnedProxyRegistrations)
                .Single(x => x.Id == id);
            if (proxyRegistration == null)
            {
                return HttpNotFound();
            }
            return View(new ProxyRegistrationVm(proxyRegistration));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,EntryPointUrl")] ProxyRegistrationVm postModel)
        {
            if (ModelState.IsValid)
            {
                var userId = HttpContext.User.Identity.GetUserId();
                var proxyRegistration = db.Users
                    .Where(u => u.Id == userId)
                    .SelectMany(u => u.OwnedProxyRegistrations)
                    .Single(x => x.Id == postModel.Id);
                proxyRegistration.UpdateDetails(postModel.Name, postModel.EntryPointUrl);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(postModel);
        }

        
 

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public class ProxyRegistrationVm
        {
            public ProxyRegistrationVm() { }
            public ProxyRegistrationVm(ProxyRegistration proxyRegistration)
            {
                this.Name = proxyRegistration.Name;
                this.EntryPointUrl = proxyRegistration.EntryPointUrl;
                this.Id = proxyRegistration.Id;
            }

            public string Name { get; set; }

            public string EntryPointUrl { get; set; }

            [DataType("Hidden")]
            public string Id { get; set; }
        }
    }
}
