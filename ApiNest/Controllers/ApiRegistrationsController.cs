﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ApiNest.Domain;
using Microsoft.AspNet.Identity;

namespace ApiNest.Controllers
{

    public class ApiRegistrationsController : Controller
    {
        private IApiNestSession db = new ApiNestContext();

        // GET: ApiRegistrations
        public ActionResult Index()
        {
            return View(db.ApiRegistrations.ToList());
        }

        // GET: ApiRegistrations/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApiRegistration apiRegistration = db.ApiRegistrations.Single(ar => ar.Id == id);
            if (apiRegistration == null)
            {
                return HttpNotFound();
            }
            return View(new ApiRegistrationVm(apiRegistration));
        }

        // GET: ApiRegistrations/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "Name,EntryPointUrl")] ApiRegistrationVm postModel)
        {


            if (ModelState.IsValid)
            {
                var userId = HttpContext.User.Identity.GetUserId();
                var currentUser = db.Users.Single(u => u.Id == userId);
                db.AddApiRegistration(postModel.Name, postModel.EntryPointUrl, currentUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(postModel);
        }

        [Authorize]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userId = HttpContext.User.Identity.GetUserId();
            ApiRegistration apiRegistration = db.Users
                .Where(u => u.Id == userId)
                .SelectMany(u => u.OwnedApiRegistrations)
                .Single(x => x.Id == id);
            if (apiRegistration == null)
            {
                return HttpNotFound();
            }
            return View(new ApiRegistrationVm(apiRegistration));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ApiRegistrationVm postModel)
        {
            if (ModelState.IsValid)
            {
                var userId = HttpContext.User.Identity.GetUserId();
                db.Users
                    .Where(u => u.Id == userId)
                    .SelectMany(u => u.OwnedApiRegistrations)
                    .Single(x => x.Id == postModel.Id)
                    .UpdateDetails(postModel.Name, postModel.EntryPointUrl);

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(postModel);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public class ApiRegistrationVm
        {
            public ApiRegistrationVm()
            {
            }

            public ApiRegistrationVm(ApiRegistration apiRegistration)
            {
                this.Name = apiRegistration.Name;
                this.EntryPointUrl = apiRegistration.EntryPointUrl;
                this.Id = apiRegistration.Id;
            }

            public string Name { get; set; }

            public string EntryPointUrl { get; set; }

            [DataType("Hidden")]
            public string Id { get; set; }
        }

    }
}
