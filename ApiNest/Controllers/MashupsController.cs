﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ApiNest.Domain;
using Microsoft.AspNet.Identity;

namespace ApiNest.Controllers
{
    public class MashupsController : Controller
    {
        private IApiNestSession db = new ApiNestContext();

        // GET: Mashups
        public ActionResult Index()
        {
            return View(db.Mashups.ToList());
        }

        // GET: Mashups/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mashup mashup = db.Mashups.Single(x => x.Id == id);
            if (mashup == null)
            {
                return HttpNotFound();
            }
            return View(mashup);
        }

        // GET: Mashups/Create
        public ActionResult Create()
        {
            return View(new MashupVm(new Mashup(), db));
        }

        // POST: Mashups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create(MashupVm mashupVm)
        {
            if (ModelState.IsValid)
            {


               
                var userId = HttpContext.User.Identity.GetUserId();
                var currentUser = db.Users.Single(u => u.Id == userId);
                var targetApi = db.ApiRegistrations.Single(ar => ar.Id == mashupVm.TargetApiId);
                var proxies = mashupVm.MiddlewareProxies.Select(mp => mp.ProxyId).ToArray()
                   .Join(db.ProxyRegistrations, s => s, pr => pr.Id,
                       (s, registration) => registration).ToArray();

                var mashup = db.AddMashup(mashupVm.Name, targetApi, proxies, currentUser);

                db.SaveChanges();
                return RedirectToAction("Details", new {id = mashup.Id});
            }

            return View(mashupVm);
        }

        // GET: Mashups/Edit/5
        [Authorize]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mashup mashup = db.Mashups.Single(x => x.Id == id);
            if (mashup == null)
            {
                return HttpNotFound();
            }
            return View(new MashupVm(mashup, db));
        }

        // POST: Mashups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit(MashupVm postModel)
        {
            if (ModelState.IsValid)
            {
                var userId = HttpContext.User.Identity.GetUserId();
                var mashup = db.Users.Where(u => u.Id == userId)
                    .SelectMany(u => u.OwnedMashups).Single(m => m.Id == postModel.Id);
                var targetApi = db.ApiRegistrations.Single(ar => ar.Id == postModel.TargetApiId);
                var proxies = postModel.MiddlewareProxies.Select(mp => mp.ProxyId).ToArray()
                   .Join(db.ProxyRegistrations, s => s, pr => pr.Id,
                       (s, registration) => registration).ToArray();
                mashup.UpdateDetails(postModel.Name, targetApi, proxies);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(postModel);
        }

      

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Visit(string id)
        {
            Mashup mashup = db.Mashups.Single(x => x.Id == id);
             
            var apiUri = new Uri(mashup.TargetApi.EntryPointUrl);
            var uri = $"http://{mashup.Id.Replace("-", "")}.{Request.Url.Host.TrimEnd('/')}/{apiUri.PathAndQuery.TrimStart('/')}";
            return Redirect(uri);
        }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        public class MashupVm
        {
            public MashupVm(Mashup mashup, IApiNestSession context)
            {
                this.Name = mashup.Name;
                this.Id = mashup.Id;
                this.TargetApiId = mashup.TargetApi?.Id;
                this.TargetApiId_choices = context.ApiRegistrations.ToArray().Select(ar => Tuple.Create(ar.Name, ar.Id)).ToArray();
                this.MiddlewareProxies =
                    mashup.MiddlewareProxies.Select(mp => new MiddlewareProxyVm(mp.Proxy)).ToArray();
            }

            public MashupVm()
            {
                MiddlewareProxies= new List<MiddlewareProxyVm>();
            }
            [DataType("Hidden")]
            public string Id { get; set; }

            public string Name { get; set; }

            [DisplayName("TargetApi")]
            public string TargetApiId { get; set; }
            public Tuple<string, string>[] TargetApiId_choices { get; set; }




            public ICollection<MiddlewareProxyVm> MiddlewareProxies { get; set; }

            public class MiddlewareProxyVm
            {
                public MiddlewareProxyVm(ProxyRegistration mp)
                {
                    ProxyId = mp.Name;
                }
                public MiddlewareProxyVm() { }
                [Required]
                public string ProxyId { get; set; }

                public Tuple<string, string>[] ProxyId_choices => ProxyIdChoices();

                private static Tuple<string, string>[] ProxyIdChoices()
                {
                    return new ApiNestContext().ProxyRegistrations.ToArray().Select(pr => Tuple.Create(pr.Name, pr.Id)).ToArray();
                }
            }
        }
    }
}