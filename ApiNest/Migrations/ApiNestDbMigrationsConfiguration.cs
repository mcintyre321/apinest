using System.Data.Entity.Migrations;
using ApiNest.Domain;

namespace ApiNest.Migrations
{
    internal sealed class ApiNestDbMigrationsConfiguration : DbMigrationsConfiguration<ApiNestContext>
    {
        public ApiNestDbMigrationsConfiguration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ApiNest.Domain.ApiNestContext";
        }

        protected override void Seed(ApiNestContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
