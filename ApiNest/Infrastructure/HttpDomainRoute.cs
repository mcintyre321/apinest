﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http.Routing;
using System.Web.Http.WebHost;

namespace ApiNest.Infrastructure
{
    public class HttpDomainRoute
        : HttpRoute
    {

        public HttpDomainRoute(string domainTemplate, string routeTemplate, object defaults, HttpMessageHandler handler, object constraints = null)
            : base(routeTemplate, new HttpRouteValueDictionary(defaults), new HttpRouteValueDictionary(constraints), new HttpRouteValueDictionary(), handler)
        {
            this.DomainTemplate = domainTemplate;
        }

        public string DomainTemplate { get; set; }


        public override IHttpRouteData GetRouteData(string virtualPathRoot, HttpRequestMessage request)
        {
            // Build regex

            // Request information
            string requestDomain = request.Headers.Host;
            if (!string.IsNullOrEmpty(requestDomain))
            {
                if (requestDomain.IndexOf(":") > 0)
                {
                    requestDomain = requestDomain.Substring(0, requestDomain.IndexOf(":"));
                }
            }
            else
            {
                requestDomain = request.RequestUri.Host;
            }
            System.Web.HttpContext.Current.Items["RequestDomain"] = requestDomain;
            
            // Match domain and route
            return DomainTemplate.Split(';')
                .Select(domainTemplate => GetHttpRouteData(domainTemplate, requestDomain))
                .FirstOrDefault(data => data != null);

        }

        private IHttpRouteData GetHttpRouteData(string domainTemplate, string requestDomain)
        {
            var templateParts = domainTemplate.Split('.');
            var hostParts = requestDomain.Split('.');

            if (templateParts.Length != hostParts.Length) return null;
            IHttpRouteData data = new HttpRouteData(this);
            ;

            if (Defaults != null)
            {
                foreach (KeyValuePair<string, object> item in Defaults)
                {
                    data.Values[item.Key] = item.Value;
                }
            }

            foreach (
                var pair in hostParts.Zip(templateParts, (hostPart, templatePart) => new {templatePart, requestPart = hostPart})
                )
            {
                if (pair.templatePart.StartsWith("{") && pair.templatePart.EndsWith("}"))
                {
                    var keyword = pair.templatePart.Substring(1, pair.templatePart.Length - 2);
                    data.Values[keyword] = pair.requestPart;
                }
                else
                {
                    //Exit if template doesn't match request
                    if (StringComparer.InvariantCultureIgnoreCase.Compare(pair.templatePart, pair.requestPart) != 0)
                    {
                        return null;
                    }
                }
            }
            return data;
        }
    }
}