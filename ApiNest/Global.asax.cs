﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ApiNest.Controllers;
using ApiNest.Domain;
using ApiNest.Infrastructure;
using ApiNest.Migrations;

namespace ApiNest
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApiNestContext, ApiNestDbMigrationsConfiguration>());


            GlobalConfiguration.Configure(config =>
            {
                config.MapHttpAttributeRoutes();
                var httpDomainRoute = new HttpDomainRoute(
                    domainTemplate: "{mashupId}.apinest.127.0.0.1.xip.io;{mashupId}.apinest.com",
                    routeTemplate: "devMashup",
                    defaults: new{ },
                    handler: new MashupMessageHandler());
                config.Routes.Add("mashupHandler", httpDomainRoute);
                //config.Routes.MapHttpRoute(
                //    name: "Route2",
                //    routeTemplate: "mashup/{id}",
                //    defaults: new {id = RouteParameter.Optional},
                //    constraints: null,
                //    handler: new MashupMessageHandler() // per-route message handler
                //    );
            });

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

        }
    }
}
